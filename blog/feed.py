from django.contrib.syndication.views import Feed
from .models import Entry

class LatestPosts(Feed):
	title = "Objective Reality Blog"
	link = "/feed"
	description = "Lates Posts From Me"

	def items(self):
		return Entry.objects.published()[:5]


